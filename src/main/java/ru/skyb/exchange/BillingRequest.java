package ru.skyb.exchange;

import ru.skyb.domain.SearchRequest;

import java.io.Serializable;

public class BillingRequest implements Serializable {
    private String method;

    private BillingUser user;

    private SearchRequest params;

    public BillingRequest(String method) {
        this.method = method;
    }

    public SearchRequest getParams() {
        return params;
    }

    public void setParams(SearchRequest params) {
        this.params = params;
    }

    public BillingUser getUser() {
        return user;
    }

    public void setUser(BillingUser user) {
        this.user = user;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
