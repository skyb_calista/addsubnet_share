package ru.skyb.data;

import com.taskadapter.redmineapi.IssueManager;
import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.RedmineManager;
import com.taskadapter.redmineapi.bean.Issue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.skyb.configure.ConfigureScheduled;
import ru.skyb.domain.SearchRequest;
import ru.skyb.exchange.BillingConnector;
import ru.skyb.exchange.RedmineConnector;
import ru.skyb.service.SendMailMessage;
import ru.skyb.utils.TimeUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class ShceduledUpdateRedmineTask {

    private final RedmineConnector redmineConnector;
    private final SendMailMessage sendMailMessage;
    private final TaskData taskData;
    private final ConfigureScheduled configureScheduled;

    private static final int SLEEP_TIME = 3600000;//Время в милисекундах(один час)
    //private static final int SLEEP_TIME = 631404;//Время в милисекундах(около 10 минут)
    //private static final int SLEEP_TIME = 3600;//Время в милисекундах(один час)
    private final static Logger logger = LoggerFactory.getLogger(ShceduledUpdateRedmineTask.class);
    //private static final long currentTimeMillis = System.currentTimeMillis();

    @Value("${create.scheduler.subnet}")
    private boolean createSchedulerSubnet;

    @Autowired
    public ShceduledUpdateRedmineTask(RedmineConnector redmineConnector, SendMailMessage sendMailMessage, TaskData taskData, ConfigureScheduled configureScheduled) {
        this.redmineConnector = redmineConnector;
        this.sendMailMessage = sendMailMessage;
        this.taskData = taskData;
        this.configureScheduled = configureScheduled;
    }

    @Scheduled(fixedRate = SLEEP_TIME)
    public void updateRedmineTask () throws RedmineException {
        logger.info("Start check task");
        //Date now = new Date();
        Calendar calendar = Calendar.getInstance();
        //calendar.add(Calendar.DAY_OF_MONTH, -1);
        logger.info("configureScheduled {}", configureScheduled.isCreateSubnetForScheduler());
        RedmineManager redmineManager = redmineConnector.updateTask();
        IssueManager issueManager = redmineManager.getIssueManager();
        SearchRequest searchRequest = new SearchRequest();
        long currentTimeMillis = System.currentTimeMillis();
        if(configureScheduled.isCreateSubnetForScheduler()) {
            List<Issue> issues = issueManager.getIssues("moscow-new-connections", null);
            for (Issue issue : issues) {
                if (issue.getAssigneeName() != null) {
                    if (issue.getAssigneeName().equals("Владимир Лопатин") && TimeUtils.dateEqual(TimeUtils.convertCalendarToDate(calendar), issue.getStartDate()) && issue.getId() != 21105) {
                        logger.info("id {} startDate {} Description {}", issue.getId(), issue.getStartDate(), issue.getDescription());

                        try {
                            searchRequest.setRedmineIssueId(String.valueOf(issue.getId()));
                            taskData.setTaskData(searchRequest, issueManager).update();
                            sendMailMessage.send( "v.l@teraline.biz", "Задача " + issue.getId() + " "
                                    + issue.getSubject(), issue.getAuthorName() + " Старый коментарий " + issue.getDescription() +
                                    "\n\nОбновленная задача\n" + issueManager.getIssueById(issue.getId()).getDescription());
                        } catch (RedmineException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        logger.info("Потраченое время {} мин.", (System.currentTimeMillis() - currentTimeMillis)/60000);
    }


}
