package ru.skyb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.skyb.configure.ConfigureScheduled;

import java.util.TimeZone;

@SpringBootApplication
@EnableScheduling
public class CreateSubnetApplication {
/*
nano /etc/sysctl.d/99-sysctl.conf
net.ipv6.conf.all.disable_ipv6 = 1
sysctl -p
 */
    public static void main(String[] args) {
        //SpringApplication.run(CreateSubnetApplication.class, args);
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Yakutsk"));
        ApplicationContext ctx = SpringApplication.run(CreateSubnetApplication.class, args);

        ConfigureScheduled configureScheduled = ctx.getBean(ConfigureScheduled.class);
        configureScheduled.setCreateSubnetForScheduler(true);
    }

}
