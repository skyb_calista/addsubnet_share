package ru.skyb.exchange;

public class BillingUser {
    private String user;
    private String pswd;

    public BillingUser(String user, String pswd) {
        this.user = user;
        this.pswd = pswd;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPswd() {
        return pswd;
    }

    public void setPswd(String pswd) {
        this.pswd = pswd;
    }
}
