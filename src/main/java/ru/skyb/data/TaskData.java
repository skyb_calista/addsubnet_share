package ru.skyb.data;

import com.taskadapter.redmineapi.IssueManager;
import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.bean.Issue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.skyb.MainController;
import ru.skyb.domain.SearchRequest;
import ru.skyb.exchange.BillingConnector;
import ru.skyb.exchange.BillingResponse;

import java.util.List;

@Component
public class TaskData {

    private final BillingConnector billingConnector;

    @Autowired
    public TaskData(BillingConnector billingConnector) {
        this.billingConnector = billingConnector;
    }

    private final static Logger logger = LoggerFactory.getLogger(MainController.class);

    public Issue setTaskData(SearchRequest searchRequest, IssueManager issueManager) throws RedmineException {
        Issue issue = issueManager.getIssueById(Integer.valueOf(searchRequest.getRedmineIssueId()));
        //IssueManager issueManager = issueManager.getIssueManager();

        //logger.info(issue.getAuthorName());
        //logger.info(""+issue.getCustomFields());
        //issue.getCustomFieldById()
        String typeSubnet = issue.getCustomFieldById(6).getValue();
        String subContract = issue.getCustomFieldById(10).getValue();
        SearchRequest redmineSearchRequest= new SearchRequest();

        if(subContract.equals("0")) {
            redmineSearchRequest.setSearchString(issue.getCustomFieldById(9).getValue());
        }
        else {
            redmineSearchRequest.setSearchString(issue.getCustomFieldById(9).getValue() + "(" + subContract + ")");
        }
        redmineSearchRequest.setCountIP(Integer.parseInt(issue.getCustomFieldById(7).getValue()));

        if(typeSubnet.equals("Внешняя(белая) сеть")) {
            redmineSearchRequest.setTypeSubnet("white");
        }
        if(typeSubnet.equals("Внутренняя(серая) сеть")) {
            redmineSearchRequest.setTypeSubnet("gray");
        }
        redmineSearchRequest.setHandle("true");
        BillingResponse billingResponse = billingConnector.search(redmineSearchRequest);
        BillingResponse.Return returnData = billingResponse.getData().getReturnData();

        issue.setDescription(issue.getDescription() + "\n\n" +
                "Договор: " + returnData.getSearchString() + "\n" +
                "Адреса абонента: "+returnData.getContractIp()+"\n" +
                "Шлюз по умолчанию: "+returnData.getGw()+"\n" +
                "Маска сети - 255.255.255.0\n" +
                "Primary DNS - 10.30.30.2\n" +
                "Secondary DNS - 10.30.30.3");
        issue.setAssigneeId(issue.getAuthorId());
        //logger.info(issue.getAuthorName());
        return issue;
    }
}
