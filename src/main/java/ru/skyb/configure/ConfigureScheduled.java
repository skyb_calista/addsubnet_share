package ru.skyb.configure;

import org.springframework.stereotype.Component;

@Component
public class ConfigureScheduled {
    private boolean createSubnetForScheduler;

    public boolean isCreateSubnetForScheduler() {
        return createSubnetForScheduler;
    }

    public void setCreateSubnetForScheduler(boolean createSubnetForScheduler) {
        this.createSubnetForScheduler = createSubnetForScheduler;
    }
}
