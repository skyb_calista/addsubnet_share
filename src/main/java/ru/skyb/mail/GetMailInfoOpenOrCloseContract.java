package ru.skyb.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.skyb.configure.ConfigureScheduled;
import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

@Component
public class GetMailInfoOpenOrCloseContract {
    private final ConfigureScheduled configureScheduled;
    private static final int SLEEP_TIME = 3600000;//Время в милисекундах(один час)
    private static final String USER = "";
    private static final String PASSWORD = "";
    private final static Logger logger = LoggerFactory.getLogger(GetMailInfoOpenOrCloseContract.class);

    @Value("${create.scheduler.subnet}")
    private boolean createSchedulerSubnet;

    @Autowired

    public GetMailInfoOpenOrCloseContract(ConfigureScheduled configureScheduled) {
        this.configureScheduled = configureScheduled;
    }
    @Scheduled(fixedRate = SLEEP_TIME)
    public void getmail () throws IOException, MessagingException {
        String host = "imap.gmail.com";
        String mailStoreType = "imap";

        //check(host, mailStoreType, USER, PASSWORD);
    }
    public static void check(String host, String storeType, String user, String password) throws IOException, MessagingException {
        try {

            // create properties
            Properties properties = new Properties();

            properties.put("mail.imap.host", host);
            properties.put("mail.imap.port", "993");
            properties.put("mail.imap.starttls.enable", "true");
            properties.put("mail.imap.ssl.trust", host);

            Session emailSession = Session.getDefaultInstance(properties);

            // create the imap store object and connect to the imap server
            Store store = emailSession.getStore("imaps");

            store.connect(host, user, password);

            // create the inbox object and open it
            Folder inbox = store.getFolder("Inbox");
            inbox.open(Folder.READ_WRITE);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));
            logger.info("messages.length---" + messages.length);
            // store attachment file name, separated by comma
            StringBuilder attachFiles = new StringBuilder();

            String messageContent = "";
            for (int i = 0, n = messages.length; i < n; i++) {
                Message message = messages[i];
                message.setFlag(Flags.Flag.SEEN, true);
                String contentType = message.getContentType();
                logger.info("---------------------------------");
                logger.info("Email Number " + (i + 1));
                logger.info("Subject: " + message.getSubject());
                StringBuilder from = new StringBuilder();
                for(Address address : message.getAllRecipients()) {
                    from.append(address.toString()).append(";");
                }
                logger.info("From: " + from.toString() + message.getFrom()[0]);

                logger.info("Text: " + getTextFromMimeMultipart((Multipart) message.getContent()));
                //logger.info("CONTENT " + message.getContent());

                if (contentType.contains("multipart")) {
                    // content may contain attachments
                    Multipart multiPart = (Multipart) message.getContent();
                    int numberOfParts = multiPart.getCount();
                    for (int partCount = 0; partCount < numberOfParts; partCount++) {
                        MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                            // this part is attachment
                            String fileName = part.getFileName();
                            attachFiles.append(fileName).append(", ");
                            part.saveFile("/home/skyb" + File.separator + fileName);
                        } else {
                            // this part may be the message content
                            messageContent = part.getContent().toString();
                        }
                    }

                    if (attachFiles.length() > 1) {
                        attachFiles = new StringBuilder(attachFiles.substring(0, attachFiles.length() - 2));
                    }
                } else if (contentType.contains("text/plain")
                        || contentType.contains("text/html")) {
                    Object content = message.getContent();
                    if (content != null) {
                        messageContent = content.toString();
                    }
                }

            }


            inbox.close(false);
            store.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String getTextFromMimeMultipart(
            Multipart mimeMultipart)  throws MessagingException, IOException{
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart){
                result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            }
        }
        return result;
    }
}
