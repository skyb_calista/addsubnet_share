package ru.skyb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class SearchRequest implements Serializable {
    private String searchString;
    private String typeSubnet;
    private int countIP;
    private String save;
    private String activate;
    private String deactivate;
    private String recalculate;
    private String month;
    private String handle;
    private String redmineIssueId;

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getTypeSubnet() {
        return typeSubnet;
    }

    public void setTypeSubnet(String typeSubnet) {
        this.typeSubnet = typeSubnet;
    }

    public int getCountIP() {
        return countIP;
    }

    public void setCountIP(int countIP) {
        this.countIP = countIP;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getActivate() {
        return activate;
    }

    public void setActivate(String activate) {
        this.activate = activate;
    }

    public String getDeactivate() {
        return deactivate;
    }

    public void setDeactivate(String deactivate) {
        this.deactivate = deactivate;
    }

    public String getRecalculate() {
        return recalculate;
    }

    public void setRecalculate(String recalculate) {
        this.recalculate = recalculate;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getRedmineIssueId() {
        return redmineIssueId;
    }

    public void setRedmineIssueId(String redmineIssueId) {
        this.redmineIssueId = redmineIssueId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("searchString", searchString)
                .append("typeSubnet", typeSubnet)
                .append("countIP", countIP)
                .toString();
    }
}
