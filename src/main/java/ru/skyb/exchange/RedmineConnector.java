package ru.skyb.exchange;

import com.taskadapter.redmineapi.RedmineManager;
import com.taskadapter.redmineapi.RedmineManagerFactory;
import org.springframework.stereotype.Component;

@Component
public class RedmineConnector {

    private static final String URI = "";
    private static final String API_ACCESS_KEY = "";

    public RedmineManager updateTask() {
        return RedmineManagerFactory.createWithApiKey(URI, API_ACCESS_KEY);
    }
}
