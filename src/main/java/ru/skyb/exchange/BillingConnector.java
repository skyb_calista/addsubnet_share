package ru.skyb.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.skyb.configure.RestTemplateConvertCharset;
import ru.skyb.domain.SearchRequest;


import java.util.Collections;

@Component
public class BillingConnector {

    private static final String USER = "";
    private static final String PSWD = "";
    private static final String METHOD = "";
    private static final String BILLING_URL = "";
    private final RestTemplateConvertCharset restTemplateConvertCharset;

    @Autowired
    public BillingConnector(RestTemplateConvertCharset restTemplateConvertCharset) {
        this.restTemplateConvertCharset = restTemplateConvertCharset;
    }

    public BillingResponse search(SearchRequest searchRequest) {
        // prepare request
        BillingRequest billingRequest = new BillingRequest(METHOD);
        billingRequest.setUser(new BillingUser(USER, PSWD));
        billingRequest.setParams(searchRequest);

        // prepare response
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        requestHeaders.setContentType(MediaType.valueOf("application/json;charset=UTF-8"));

        // send request
        HttpEntity request = new HttpEntity<>(billingRequest, requestHeaders);
        ResponseEntity<BillingResponse> responseEntity = restTemplateConvertCharset.restTemplate().postForEntity(BILLING_URL, request, BillingResponse.class);

        return responseEntity.getBody();
    }
}
