package ru.skyb.utils;

import java.sql.Timestamp;
import java.text.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class TimeUtils {

    public static final String[] monthNames = new String[]{"январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};
    public static final String[] monthNamesRod = new String[]{"января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"};
    public static final String[] weekDayName = new String[]{"Пн.", "Вт.", "Ср.", "Чт.", "Пт.", "Сб.", "Вс."};
    public static final String DATE_FORMAT_PATTERN_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_PATTERN_YYYY_MM_DD_HHMMSS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_PATTERN_DDMMYYYY = "dd.MM.yyyy";
    public static final String DATE_FORMAT_PATTERN_DDMMYYYY_HHMMSS = "dd.MM.yyyy HH:mm:ss";
    public static final String DATE_FORMAT_PATTERN_DDMMYYYY_HHMM = "dd.MM.yyyy HH:mm";

    public TimeUtils() {
    }

    public static final int getDayOfWeekMask(Calendar date) {
        return 1 << (date.get(7) - 2 + 7) % 7;
    }

    public static final int getDayOfMonthMask(Calendar date) {
        return 1 << date.get(5) - 1;
    }

    public static final int getHourOfDayMask(Calendar date) {
        return 1 << date.get(11);
    }

    public static final int getMonthOfYearMask(Calendar date) {
        return 1 << date.get(2);
    }

    public static final long getMinuteMask(Calendar date) {
        return 1L << date.get(12);
    }

    public static final boolean checkMasks(final Calendar date, final int hourOfDay, final int dayOfWeek, final int monthOfYear, final int dayOfMonth) {
        if (hourOfDay > 0 && (hourOfDay & getHourOfDayMask(date)) == 0) {
            return false;
        } else if (dayOfWeek > 0 && (dayOfWeek & getDayOfWeekMask(date)) == 0) {
            return false;
        } else if (dayOfMonth > 0 && (dayOfMonth & getDayOfMonthMask(date)) == 0) {
            return false;
        } else if (monthOfYear > 0 && (monthOfYear & getMonthOfYearMask(date)) == 0) {
            return false;
        } else {
            return dayOfWeek <= 0 || (dayOfWeek & getDayOfWeekMask(date)) != 0;
        }
    }

    public static final boolean checkMasks(Calendar date, int hourOfDay, int dayOfWeek, int monthOfYear, int dayOfMonth, long minute) {
        boolean result = checkMasks(date, hourOfDay, dayOfWeek, monthOfYear, dayOfMonth);
        if (result && minute > 0L) {
            result = (minute & getMinuteMask(date)) > 0L;
        }

        return result;
    }

    public static final String convertCalendarToDateString(Calendar calendar) {
        String stringDate = format(calendar, "yyyy-MM-dd");
        if (stringDate == null) {
            stringDate = "0000-00-00";
        }

        return stringDate;
    }

    public static final String convertCalendarToDateTimeString(Calendar calendar) {
        String stringDate = format(calendar, "yyyy-MM-dd HH:00:00");
        if (stringDate == null) {
            stringDate = "0000-00-00 00:00:00";
        }

        return stringDate;
    }

    public static final String format(Date date, String pattern) {
        return date == null ? "" : getDateFormat(pattern).format(date);
    }

    public static final String format(Date date, String pattern, TimeZone zone) {
        return date == null ? "" : getDateFormat(pattern, zone).format(date);
    }

    public static final String format(Date date, DateFormat format) {
        return date == null ? "" : format.format(date);
    }

    public static final String format(Calendar date, String pattern) {
        return date == null ? "" : getDateFormat(pattern).format(date.getTime());
    }

    public static final String format(Calendar date, DateFormat format) {
        return date == null ? "" : format.format(date.getTime());
    }

    public static final String formatTimestamp(Timestamp time, String format) {
        String result = "";
        if (time != null && format != null) {
            result = getDateFormat(format).format(new java.sql.Date(time.getTime()));
        }

        return result;
    }

    public static final String formatDate(Calendar date) {
        return format(date, "dd.MM.yyyy");
    }

    public static final String formatDate(Date date) {
        return format(date, "dd.MM.yyyy");
    }

    public static final String formatSQLDate(Date date) {
        return format(date, "yyyy-MM-dd");
    }

    public static final String formatSQLDate(Calendar date) {
        return format(date, "yyyy-MM-dd");
    }

    public static final String formatPeriod(Calendar date1, Calendar date2) {
        return formatPeriod(date1 != null ? date1.getTime() : null, date2 != null ? date2.getTime() : null);
    }

    public static final String formatPeriod(Date date1, Date date2) {
        return (date1 != null ? formatDate(date1) : "…") + "-" + (date2 != null ? formatDate(date2) : "…");
    }

/*    public static final String formatPeriod(Period period) {
        return formatPeriod(period.getDateFrom(), period.getDateTo());
    }

    public static final String formatPeriodWithTime(Date date1, Date date2) {
        return (date1 != null ? formatFullDate(date1) : "…") + " - " + (date2 != null ? formatFullDate(date2) : "…");
    }

    public static final String formatPeriodWithTime(Period period) {
        return formatPeriodWithTime(period.getDateFrom(), period.getDateTo());
    }
*/
    public static String formatFullDate(Date inDate) {
        String outDate = "";
        if (inDate != null) {
            try {
                outDate = getDateFormat("dd.MM.yyyy HH:mm:ss").format(inDate);
            } catch (Exception var3) {
                var3.printStackTrace();
            }
        }

        return outDate;
    }

    public static final String formatDeltaTime(long delta) {
        int days = (int)(delta / 86400L);
        delta -= (long)(days * 86400);
        int hours = (int)(delta / 3600L);
        delta -= (long)(hours * 3600);
        int min = (int)(delta / 60L);
        delta -= (long)(min * 60);
        int sec = (int)delta;
        StringBuffer result = new StringBuffer(30);
        DecimalFormat dfTime = new DecimalFormat("00");
        result.append(days);
        result.append(" d ");
        result.append(dfTime.format((long)hours));
        result.append(":");
        result.append(dfTime.format((long)min));
        result.append(":");
        result.append(dfTime.format((long)sec));
        return result.toString();
    }

    public static final Date convertCalendarToDate(Calendar calendar) {
        return calendar != null ? calendar.getTime() : null;
    }

    public static final Date convertSqlDateToDate(java.sql.Date date) {
        Date outDate = null;
        if (date != null) {
            outDate = new Date(date.getTime());
        }

        return outDate;
    }

    public static Date parseDate(String str, String format) {
        return parseDate(str, getDateFormat(format));
    }

    public static Date parseDate(String str, String format, TimeZone zone) {
        return parseDate(str, getDateFormat(format, zone));
    }

    public static Date parseDate(String str, DateFormat format) {
        Date date = null;
        if (str != null && format != null) {
            try {
                date = format.parse(str);
            } catch (Exception var4) {
            }
        }

        return date;
    }

    public static final Calendar convertDateToCalendar(Date date) {
        Calendar result = null;
        if (date != null) {
            result = new GregorianCalendar();
            result.setTime(date);
        }

        return result;
    }

    public static final Calendar convertStringToCalendar(String param) {
        Calendar result = null;
        if (param != null && param.length() > 0 && !"00.00.0000".equals(param)) {
            result = convertStringToCalendar(param, "dd.MM.yyyy");
            if (result == null) {
                result = convertStringToCalendar(param, "yyyy-MM-dd");
            }
        }

        return result;
    }

    public static final Date convertStringToDate(String param) {
        Date result = convertStringToDate(param, "dd.MM.yyyy");
        if (result == null) {
            result = convertStringToDate(param, "yyyy-MM-dd");
        }

        return result;
    }

    public static final Calendar convertStringToCalendar(String param, String pattern) {
        Calendar calendar = null;
        if (param != null && pattern != null) {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat(pattern);
                calendar = new GregorianCalendar();
                calendar.setTime(formatter.parse(param));
            } catch (ParseException var4) {
            }
        }

        return calendar;
    }

    public static final Date convertStringToDate(String param, String pattern) {
        Date date = null;
        if (param != null && pattern != null) {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat(pattern);
                date = formatter.parse(param);
            } catch (ParseException var4) {
            }
        }

        return date;
    }

    public static final Calendar convertFullStringToCalendar(String param) {
        return convertStringToCalendar(param, "dd.MM.yyyy HH':00:00'");
    }

    public static final Calendar convertTimestampToCalendar(Timestamp time) {
        Calendar result = null;
        if (time != null) {
            result = new GregorianCalendar();
            result.setTimeInMillis(time.getTime());
        }

        return result;
    }

    public static final Date convertTimestampToDate(Timestamp time) {
        Date result = null;
        if (time != null) {
            result = new Date(time.getTime());
        }

        return result;
    }

    public static Calendar parseCalendar(String str, String format) {
        Date date = parseDate(str, format);
        if (date != null) {
            Calendar result = new GregorianCalendar();
            result.setTime(date);
            return result;
        } else {
            return null;
        }
    }

    public static Calendar parseCalendar(String str, DateFormat format) {
        Calendar result = null;
        Date date = parseDate(str, format);
        if (date != null) {
            result = new GregorianCalendar();
            result.setTime(date);
        }

        return result;
    }

    public static final Calendar clear_MIN_MIL_SEC(Calendar time) {
        time.set(14, 0);
        time.set(13, 0);
        time.set(12, 0);
        return time;
    }

    public static final Date clear_MIN_MIL_SEC(Date time) {
        time = convertCalendarToDate(clear_MIN_MIL_SEC(convertDateToCalendar(time)));
        return time;
    }

    public static final Calendar clear_HOUR_MIN_MIL_SEC(Calendar time) {
        clear_MIN_MIL_SEC(time);
        time.set(11, 0);
        return time;
    }

    public static final Date clear_HOUR_MIN_MIL_SEC(Date time) {
        Calendar cal = convertDateToCalendar(time);
        clear_MIN_MIL_SEC(cal);
        cal.set(11, 0);
        return convertCalendarToDate(cal);
    }

    public static final Date clear_MILLISECOND(Date time) {
        Calendar cal = convertDateToCalendar(time);
        cal.set(14, 0);
        return convertCalendarToDate(cal);
    }

    public static final Calendar getEndDay(Calendar date) {
        Calendar result = (Calendar)date.clone();
        result.set(11, 23);
        result.set(12, 59);
        result.set(13, 59);
        result.set(14, 0);
        return result;
    }

    public static final Date getEndDay(Date date) {
        return getEndDay(convertDateToCalendar(date)).getTime();
    }

    public static final Calendar getStartMonth(Calendar date) {
        Calendar result = (Calendar)date.clone();
        result.set(5, 1);
        return result;
    }

    public static final Date getStartMonth(Date date) {
        return getStartMonth(convertDateToCalendar(date)).getTime();
    }

    public static final Date getEndMonth(Date date) {
        return getEndMonth(convertDateToCalendar(date)).getTime();
    }

    public static final Calendar getEndMonth(Calendar date) {
        Calendar result = (Calendar)date.clone();
        result.set(5, date.getActualMaximum(5));
        return result;
    }

    public static final Calendar convertSqlDateToCalendar(java.sql.Date date) {
        return convertDateToCalendar(convertSqlDateToDate(date));
    }

    public static final Calendar getNextDay(Calendar date) {
        date = (Calendar)date.clone();
        moveToStartNextDay(date);
        return date;
    }

    public static final Calendar getNextMonth(Calendar date) {
        Calendar result = null;
        if (date != null) {
            result = (Calendar)date.clone();
            result.add(2, 1);
            result.set(11, 0);
            result.set(12, 0);
            result.set(13, 0);
            result.set(14, 0);
        }

        return result;
    }

    public static final Date getNextMonth(Date date) {
        return convertCalendarToDate(getNextMonth(convertDateToCalendar(date)));
    }

    public static final Calendar getPrevDay(Calendar date) {
        date = (Calendar)date.clone();
        date.add(6, -1);
        return date;
    }

    public static final Date getPrevDay(Date date) {
        return convertCalendarToDate(getPrevDay(convertDateToCalendar(date)));
    }

    public static final Date getNextDay(Date date) {
        return convertCalendarToDate(getNextDay(convertDateToCalendar(date)));
    }

    public static final java.sql.Date convertCalendarToSqlDate(Calendar calendar) {
        java.sql.Date result = null;
        if (calendar != null) {
            result = new java.sql.Date(calendar.getTimeInMillis());
        }

        return result;
    }

    public static final java.sql.Date convertDateToSqlDate(Date date) {
        return date != null ? new java.sql.Date(date.getTime()) : null;
    }

    public static final java.sql.Date convertSQLDate(String inDate) {
        java.sql.Date outDate = null;
        if (inDate != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                outDate = new java.sql.Date(sdf.parse(inDate).getTime());
            } catch (Exception var3) {
            }
        }

        return outDate;
    }

    public static final Timestamp convertCalendarToTimestamp(Calendar calendar) {
        Timestamp result = null;
        if (calendar != null) {
            result = new Timestamp(calendar.getTimeInMillis());
        }

        return result;
    }

    public static final Timestamp convertDateToTimestamp(Date date) {
        Timestamp result = null;
        if (date != null) {
            result = new Timestamp(date.getTime());
        }

        return result;
    }

    public static final Timestamp convertDateToTimestampSeconds(final Date date) {
        return date != null ? new Timestamp(date.getTime() / 1000L * 1000L) : null;
    }

    public static final Timestamp convertLongToTimestamp(long millis) {
        return new Timestamp(millis);
    }

    public static final Timestamp convertLongToTimestamp(Long millis) {
        Timestamp result = null;
        if (millis != null) {
            result = new Timestamp(millis);
        }

        return result;
    }

    public static final DateFormat getDateFormat(String pattern, TimeZone zone) {
        SimpleDateFormat dateFormat = null;

        try {
            dateFormat = new SimpleDateFormat(pattern);
        } catch (Exception var4) {
            var4.printStackTrace();
            dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        }

        if (zone != null) {
            dateFormat.setTimeZone(zone);
        }

        return dateFormat;
    }

    public static final DateFormat getDateFormat(String pattern) {
        return getDateFormat(pattern, (TimeZone)null);
    }

    public static final int daysDelta(Calendar dayFrom, Calendar dayTo) {
        dayFrom = (Calendar)dayFrom.clone();
        clear_HOUR_MIN_MIL_SEC(dayFrom);
        dayTo = (Calendar)dayTo.clone();
        clear_HOUR_MIN_MIL_SEC(dayTo);
        long time1 = dayFrom.getTimeInMillis();
        long time2 = dayTo.getTimeInMillis();
        int days1 = (int)(time1 / 86400000L);
        int days2 = (int)(time2 / 86400000L);
        return days2 - days1;
    }

    public static final int daysDelta(Date dateFrom, Date dateTo) {
        return daysDelta(convertDateToCalendar(dateFrom), convertDateToCalendar(dateTo));
    }

    public static final int hourDelta(Calendar hourFrom, Calendar hourTo) {
        long delta = (hourTo.getTimeInMillis() - hourFrom.getTimeInMillis()) / 1000L;
        return (int)(delta / 3600L);
    }

    public static final int monthsDelta(Date dateFrom, Date dateTo) {
        return monthsDelta(convertDateToCalendar(dateFrom), convertDateToCalendar(dateTo));
    }

    public static final int monthsDelta(Calendar dateFrom, Calendar dateTo) {
        return (dateTo.get(1) - dateFrom.get(1)) * 12 + (dateTo.get(2) - dateFrom.get(2));
    }

    public static final float getPart(long fromDate, long toDate, long periodFrom, long periodTo) {
        float part = 1.0F;
        if (fromDate == 0L || fromDate < periodFrom) {
            fromDate = periodFrom;
        }

        if (toDate == 0L || periodTo < toDate) {
            toDate = periodTo;
        }

        Calendar fromCalendar = new GregorianCalendar();
        Calendar toCalendar = new GregorianCalendar();
        fromCalendar.setTimeInMillis(fromDate);
        toCalendar.setTimeInMillis(toDate);
        int days = daysDelta((Calendar)fromCalendar, (Calendar)toCalendar) + 1;
        fromCalendar.setTimeInMillis(periodFrom);
        toCalendar.setTimeInMillis(periodTo);
        part = (float)days / (float)(daysDelta((Calendar)fromCalendar, (Calendar)toCalendar) + 1);
        return part;
    }

    public static final float getPart(Calendar fromDate, Calendar toDate, Calendar periodFrom, Calendar periodTo) {
        float part = 1.0F;
        if (fromDate == null || dateBefore(fromDate, periodFrom)) {
            fromDate = periodFrom;
        }

        if (toDate == null || dateBefore(periodTo, toDate)) {
            toDate = periodTo;
        }

        int days = daysDelta(fromDate, toDate) + 1;
        part = (float)days / (float)(daysDelta(periodFrom, periodTo) + 1);
        return part;
    }

    public static boolean dateBefore(final Calendar date1, final Calendar date2) {
        if (date1 != null && date2 != null) {
            if (date1.get(1) == date2.get(1)) {
                return date1.get(6) < date2.get(6);
            } else {
                return date1.get(1) < date2.get(1);
            }
        } else {
            return false;
        }
    }

    public static boolean dateBefore(final Date date1, final Date date2) {
        return dateBefore(convertDateToCalendar(date1), convertDateToCalendar(date2));
    }

    public static boolean dateEqual(Calendar date1, Calendar date2) {
        return date1 == date2 || date1 != null && date2 != null && date1.get(6) == date2.get(6) && date1.get(1) == date2.get(1);
    }

    public static boolean dateEqual(Date date1, Date date2) {
        return dateEqual(convertDateToCalendar(date1), convertDateToCalendar(date2));
    }

    public static boolean dateHourEqual(Calendar dtime1, Calendar dtime2) {
        return dateEqual(dtime1, dtime2) && dtime1.get(11) == dtime2.get(11);
    }

    public static boolean dateBeforeOrEq(Calendar date1, Calendar date2) {
        return dateBefore(date1, date2) || dateEqual(date1, date2);
    }

    public static boolean dateBeforeOrEq(Date date1, Date date2) {
        return dateBeforeOrEq(convertDateToCalendar(date1), convertDateToCalendar(date2));
    }

    public static final boolean dateInRange(Calendar checking, Calendar date1, Calendar date2) {
        return checking != null && (date1 == null || dateBeforeOrEq(date1, checking)) && (date2 == null || dateBeforeOrEq(checking, date2));
    }

    public static final boolean dateInRange(Date checking, Date date1, Date date2) {
        return checking != null && (date1 == null || dateBeforeOrEq(date1, checking)) && (date2 == null || dateBeforeOrEq(checking, date2));
    }

    public static final boolean dateInRangeWithTime(Date checking, Date date1, Date date2) {
        boolean result = false;
        if (checking != null) {
            result = true;
            if (date1 != null) {
                result = date1.getTime() <= checking.getTime();
            }

            if (result && date2 != null) {
                result = checking.getTime() <= date2.getTime();
            }
        }

        return result;
    }

/*    public static final boolean dateInPeriod(Date checking, Period period) {
        return checking != null && period != null && (period.getDateFrom() == null || period.getDateFrom().getTime() <= checking.getTime()) && (period.getDateTo() == null || checking.getTime() <= period.getDateTo().getTime());
    }

    public static final boolean dateInPeriod(LocalDate checking, Period period) {
        return checking != null && period != null && (period.getLocalDateFrom() == null || period.getLocalDateFrom().compareTo(checking) < 1) && (period.getLocalDateTo() == null || checking.compareTo(period.getLocalDateTo()) < 1);
    }
*/
    public static final LocalDate convertDateToLocalDate(Date date) {
        return date == null ? null : LocalDate.from(Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()));
    }

    public static final Date convertLocalDateToDate(LocalDate localDate) {
        return localDate == null ? null : Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static final LocalDateTime convertSecEpochToLocalDateTime(long secEpoch) {
        return LocalDateTime.from(Instant.ofEpochSecond(secEpoch).atZone(ZoneId.systemDefault()));
    }

    public static final boolean periodInRange(Calendar checkingDate1, Calendar checkingDate2, Calendar date1, Calendar date2) {
        boolean result = date1 == null || checkingDate1 != null && dateBeforeOrEq(date1, checkingDate1);
        if (result) {
            result = date2 == null || checkingDate2 != null && dateBeforeOrEq(checkingDate2, date2);
        }

        return result;
    }

    public static final boolean periodInRange(Date checkingDate1, Date checkingDate2, Date date1, Date date2) {
        boolean result = date1 == null || checkingDate1 != null && dateBeforeOrEq(date1, checkingDate1);
        if (result) {
            result = date2 == null || checkingDate2 != null && dateBeforeOrEq(checkingDate2, date2);
        }

        return result;
    }

    public static final void moveToEndOfMonth(Calendar date) {
        if (date != null) {
            date.set(5, date.getActualMaximum(5));
        }

    }

    public static final void moveToStartNextDay(Calendar date) {
        if (date != null) {
            date.add(6, 1);
            date.set(11, 0);
            date.set(12, 0);
            date.set(13, 0);
            date.set(14, 0);
        }

    }

    public static final Date moveToEndDay(Date date) {
        if (date != null) {
            Calendar dt = Calendar.getInstance();
            dt.setTime(date);
            moveToEndDay(dt);
            return dt.getTime();
        } else {
            return null;
        }
    }

    public static final void moveToEndDay(Calendar date) {
        if (date != null) {
            date.set(11, 23);
            date.set(12, 59);
            date.set(13, 59);
            date.set(14, 999);
        }

    }

    public static void clearCalendarHour(Calendar cal) {
        if (cal != null) {
            cal.clear(14);
            cal.clear(13);
            cal.clear(12);
            cal.set(11, 0);
        }

    }

    public static Date nowPlusPeriod(int period, int value) {
        Calendar c = Calendar.getInstance();
        c.add(period, value);
        return c.getTime();
    }

    public static Calendar plusPeriod(Calendar date, int period, int value) {
        Calendar result = (Calendar)date.clone();
        result.add(period, value);
        return result;
    }

    public static Date plusPeriod(Date date, int period, int value) {
        return convertCalendarToDate(plusPeriod(convertDateToCalendar(date), period, value));
    }

    public static boolean checkDateIntervalsIntersection(Date date1, Date date2, Date dateFrom, Date dateTo) {
        if ((date1 != null || date2 != null) && (dateFrom != null || dateTo != null)) {
            Date dateOne1 = date1 == null ? convertStringToDate("01.01.0001") : date1;
            Date dateTwo1 = date2 == null ? convertStringToDate("31.12.9999") : date2;
            Date dateOne2 = dateFrom == null ? convertStringToDate("01.01.0001") : dateFrom;
            Date dateTwo2 = dateTo == null ? convertStringToDate("31.12.9999") : dateTo;
            return dateInRange(dateOne1, dateOne2, dateTwo2) || dateInRange(dateTwo1, dateOne2, dateTwo2) || dateInRange(dateOne2, dateOne1, dateTwo1) || dateInRange(dateTwo2, dateOne1, dateTwo1);
        } else {
            return true;
        }
    }

    /*    public static int compare(Calendar value, Calendar compare, int field) {
            int result = compareResult(value, compare, 1);
            if (result == 0 && field != 1) {
                if (field == 3) {
                    return compareResult(value, compare, 3);
                } else if (field == 6) {
                    return compareResult(value, compare, 6);
                } else {
                    result = compareResult(value, compare, 2);
                    if (result == 0 && field != 2) {
                        if (field == 4) {
                            return compareResult(value, compare, 4);
                        } else {
                            result = compareResult(value, compare, 5);
                            return result == 0 && field != 5 && field != 5 && field != 7 && field != 8 ? compareTime(value, compare, field) : result;
                        }
                    } else {
                        return result;
                    }
                }
            } else {
                return result;
            }
        }

        private static int compareTime(Calendar value, Calendar compare, int field) {
            int result = false;
            int result = compareResult(value, compare, 11);
            if (result == 0 && field != 10 && field != 11) {
                result = compareResult(value, compare, 12);
                if (result == 0 && field != 12) {
                    result = compareResult(value, compare, 13);
                    if (result == 0 && field != 13) {
                        if (field == 14) {
                            return compareResult(value, compare, 14);
                        } else {
                            throw new IllegalArgumentException("Invalid field: " + field);
                        }
                    } else {
                        return result;
                    }
                } else {
                    return result;
                }
            } else {
                return result;
            }
        }
    */
    private static int compareResult(Calendar value, Calendar compare, int field) {
        int difference = value.get(field) - compare.get(field);
        if (difference < 0) {
            return -1;
        } else {
            return difference > 0 ? 1 : 0;
        }
    }

    public static long clearToBeginDay(Calendar date) {
        Calendar newcal = Calendar.getInstance();
        newcal.clear();
        newcal.set(date.get(1), date.get(2), date.get(5));
        return newcal.getTimeInMillis();
    }

    public static void floor(Calendar calendar, int field) {
        switch(field) {
            case 1:
                calendar.set(14, 0);
                calendar.set(13, 0);
                calendar.set(12, 0);
                calendar.set(11, 0);
                calendar.set(5, 1);
                calendar.set(2, 0);
                break;
            case 2:
                calendar.set(14, 0);
                calendar.set(13, 0);
                calendar.set(12, 0);
                calendar.set(11, 0);
                calendar.set(5, 1);
                break;
            case 3:
                calendar.set(14, 0);
                calendar.set(13, 0);
                calendar.set(12, 0);
                calendar.set(11, 0);
                calendar.set(7, calendar.getFirstDayOfWeek());
                break;
            case 4:
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                throw new IllegalArgumentException();
            case 5:
                calendar.set(14, 0);
                calendar.set(13, 0);
                calendar.set(12, 0);
                calendar.set(11, 0);
                break;
            case 10:
            case 11:
                calendar.set(14, 0);
                calendar.set(13, 0);
                calendar.set(12, 0);
                break;
            case 12:
                calendar.set(14, 0);
                calendar.set(13, 0);
        }

    }

    public static String formatCount(int field, int count) {
        switch(field) {
            case 1:
                return MessageFormat.format("{0} {0, choice, 0#лет| 1#год| 2#года| 3#года| 4#года| 4<лет| 20<{1}}", count, MessageFormat.format("{0, choice, 0#лет| 1#год| 2#года| 3#года| 4#года| 4<лет}", count % 10));
            case 2:
                return MessageFormat.format("{0} {0, choice, 0#месяцев| 1#месяц| 2#месяца| 3#месяца| 4#месяца| 4<месяцев| 20<{1}}", count, MessageFormat.format("{0, choice, 0#месяцев| 1#месяц| 2#месяца| 3#месяца| 4#месяца| 4<месяцев}", count % 10));
            case 3:
            case 4:
                return MessageFormat.format("{0} {0, choice, 0#недель| 1#неделя| 2#недели| 3#недели| 4#недели| 4<недель| 20<{1}}", count, MessageFormat.format("{0, choice, 0#недель| 1#неделя| 2#недели| 3#недели| 4#недели| 4<недель}", count % 10));
            case 5:
                return MessageFormat.format("{0} {0, choice, 0#дней| 1#день| 2#дня| 3#дня| 4#дня| 4<дней| 20<{1}}", count, MessageFormat.format("{0, choice, 0#дней| 1#день| 2#дня| 3#дня| 4#дня| 4<дней}", count % 10));
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                throw new IllegalArgumentException();
            case 10:
            case 11:
                return MessageFormat.format("{0} {0, choice, 0#часов| 1#час| 2#часа| 3#часа| 4#часа| 4<часов| 20<{1}}", count, MessageFormat.format("{0, choice, 0#часов| 1#час| 2#часа| 3#часа| 4#часа| 4<часов}", count % 10));
            case 12:
                return MessageFormat.format("{0} {0, choice, 0#минут| 1#минута| 2#минуты| 3#минуты| 4#минуты| 4<минут| 20<{1}}", count, MessageFormat.format("{0, choice, 0#минут| 1#минута| 2#минуты| 3#минуты| 4#минуты| 4<минут}", count % 10));
        }
    }

    public static Date subtractHoursFromNow(int hours) {
        Calendar now = new GregorianCalendar();
        now.add(11, -hours);
        return now.getTime();
    }

    public static final boolean timeInRange(Date checking, Date date1, Date date2) {
        return checking != null && (date1 == null || date1.compareTo(checking) <= 0) && (date2 == null || date2.compareTo(checking) >= 0);
    }

    public static long rountToSeconds(final long millis) {
        return millis / 1000L * 1000L;
    }

    public static final long convertDateToMillisFrom(Calendar utilCalendar, Date dateFrom) {
        if (dateFrom != null) {
            utilCalendar.setTime(dateFrom);
            clear_HOUR_MIN_MIL_SEC(utilCalendar);
            return utilCalendar.getTimeInMillis();
        } else {
            return 0L;
        }
    }

    public static final long convertDateToMillisTo(Calendar utilCalendar, Date dateTo) {
        if (dateTo != null) {
            utilCalendar.setTime(dateTo);
            clear_HOUR_MIN_MIL_SEC(utilCalendar);
            utilCalendar.add(5, 1);
            return utilCalendar.getTimeInMillis() - 1L;
        } else {
            return 0L;
        }
    }

    public static final Date convertMillisToDate(long millis) {
        return millis == 0L ? null : new Date(millis);
    }

    public static final Calendar convertMillisToCalendar(long millis) {
        return convertDateToCalendar(convertMillisToDate(millis));
    }

    public static long hourToMillis(long millis) {
        return millis / 3600000L * 3600000L;
    }

    public static String formatSecondsToDayHourMinute(long seconds) {
        StringBuilder buf = new StringBuilder();
        buf.append(seconds / 60L / 60L / 24L);
        buf.append(":");
        long hour = seconds % 86400L / 60L / 60L;
        if (hour < 10L) {
            buf.append("0");
        }

        buf.append(hour);
        buf.append(":");
        long minute = seconds % 3600L / 60L;
        if (minute < 10L) {
            buf.append("0");
        }

        buf.append(minute);
        return buf.toString();
    }

    public static boolean checkMonthAndYearDates(Date date1, Date date2) {
        boolean result = false;
        if (date1 != null && date2 != null) {
            Calendar calendar1 = convertDateToCalendar(date1);
            Calendar calendar2 = convertDateToCalendar(date2);
            if (calendar1.get(1) == calendar2.get(1) && calendar1.get(2) == calendar2.get(2)) {
                result = true;
            }
        }

        return result;
    }
}

