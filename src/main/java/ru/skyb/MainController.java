package ru.skyb;

import com.taskadapter.redmineapi.IssueManager;
import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.RedmineManager;
import com.taskadapter.redmineapi.bean.Issue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.skyb.data.TaskData;
import ru.skyb.domain.SearchRequest;
import ru.skyb.domain.SearchResponse;
import ru.skyb.exchange.BillingConnector;
import ru.skyb.exchange.BillingResponse;
import ru.skyb.exchange.RedmineConnector;

@Controller
public class MainController {

    private final static Logger logger = LoggerFactory.getLogger(MainController.class);

    private final BillingConnector billingConnector;
    private final RedmineConnector redmineConnector;
    private final TaskData taskData;

    @Autowired
    public MainController(BillingConnector billingConnector, RedmineConnector redmineConnector, TaskData taskData) {
        this.billingConnector = billingConnector;
        this.redmineConnector = redmineConnector;
        this.taskData = taskData;
    }

    @GetMapping(value = {"/", "/index"})
    public String index() {
        return "index";
    }

    @RequestMapping(
            value = "/subscriberSettings",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = {"application/json;**charset=UTF-8**"}
            )
    //public ResponseEntity<SearchResponse> search(ModelMap model, @ModelAttribute SearchRequest searchRequest)  {
    public String search(ModelMap model, @ModelAttribute SearchRequest searchRequest, @RequestParam(value = "searchString") String searchString) {
        //logger.info("searchRequest: {}", searchRequest.getMonth());
        BillingResponse billingResponse = billingConnector.search(searchRequest);

        if(searchRequest.getSave() != null) {

            BillingResponse.Return returnData = billingResponse.getData().getReturnData();

            SearchResponse searchResponse = new SearchResponse();
            searchResponse.setContractIp(returnData.getContractIp());
            searchResponse.setGw(returnData.getGw());
            searchResponse.setSearchString(returnData.getSearchString());


            model.addAttribute("searchString", returnData.getSearchString());
            model.addAttribute("contractIp", returnData.getContractIp());
            model.addAttribute("gw", returnData.getGw());

            return "searchContract";
        }
        if (searchRequest.getRecalculate() != null) {
            model.addAttribute("inputSearchString", searchString);
            return "index";
        }
        if(searchRequest.getActivate() != null || searchRequest.getDeactivate() != null) {
            return "redirect:/";
        }
        if(searchRequest.getHandle() != null) {

            RedmineManager redmineManager = redmineConnector.updateTask();
            IssueManager issueManager = redmineManager.getIssueManager();

            try {
                taskData.setTaskData(searchRequest, issueManager).update();

            } catch (RedmineException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/";
    }
}
