package ru.skyb.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.skyb.configure.ConfigureScheduled;

@Controller
public class SetConfData {

    private final ConfigureScheduled configureScheduled;

    private static final Logger log = LoggerFactory.getLogger(SetConfData.class);
    @Value("${create.scheduler.subnet}")
    private boolean createSchedulerSubnet;

    public SetConfData(ConfigureScheduled configureScheduled) {
        this.configureScheduled = configureScheduled;
    }

    @PostMapping("/setConfData")
    public String setConfData(Model model, @RequestParam(value = "task") String task) {

        log.info("configureScheduled {}", task);
        configureScheduled.setCreateSubnetForScheduler(Boolean.parseBoolean(task));
        model.addAttribute("task", configureScheduled.isCreateSubnetForScheduler());
        return  "index";
    }
}
