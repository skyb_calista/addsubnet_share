package ru.skyb.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class BillingResponse implements Serializable {

    private String status;
    private String exception;
    private String message;
    private String tag;
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @JsonProperty("return")
        private Return returnData;

        public Return getReturnData() {
            return returnData;
        }

        public void setReturnData(Return returnData) {
            this.returnData = returnData;
        }
    }

    public static class Return {
        private String searchString;
        private String contractIp;
        private String gw;

        public String getSearchString() {
            return searchString;
        }

        public void setSearchString(String searchString) {
            this.searchString = searchString;
        }

        public String getContractIp() {
            return contractIp;
        }

        public void setContractIp(String contractIp) {
            this.contractIp = contractIp;
        }

        public String getGw() {
            return gw;
        }

        public void setGw(String gw) {
            this.gw = gw;
        }
    }
}
