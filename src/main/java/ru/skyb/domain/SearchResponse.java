package ru.skyb.domain;

import java.io.Serializable;

public class SearchResponse implements Serializable {
    private String searchString;
    private String contractIp;
    private String gw;

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getContractIp() {
        return contractIp;
    }

    public void setContractIp(String contractIp) {
        this.contractIp = contractIp;
    }

    public String getGw() {
        return gw;
    }

    public void setGw(String gw) {
        this.gw = gw;
    }
}
